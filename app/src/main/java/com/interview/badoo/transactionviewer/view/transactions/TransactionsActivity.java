package com.interview.badoo.transactionviewer.view.transactions;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.interview.badoo.transactionviewer.App;
import com.interview.badoo.transactionviewer.R;
import com.interview.badoo.transactionviewer.view.transactions.di.DaggerTransactionComponent;
import com.interview.badoo.transactionviewer.view.transactions.model.TransactionViewModel;

import java.util.Collection;

import javax.inject.Inject;

public class TransactionsActivity extends AppCompatActivity implements TransactionsMVP.View {

    public static final String ARG_SKU = "ARG_SKU";

    @Inject TransactionsMVP.Presenter presenter;
    private RecyclerView recyclerView;
    private TextView totalView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependencies();
        String sku = getIntent().getStringExtra(ARG_SKU);
        initLayout(sku);

        presenter.init(sku);
    }

    private void initLayout(String sku) {
        setContentView(R.layout.transactions_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Transactions for " + sku);
        }
        recyclerView = ((RecyclerView) findViewById(R.id.item_list));
        totalView = ((TextView) findViewById(R.id.total));
    }

    private void initDependencies() {
        DaggerTransactionComponent.builder()
                                  .singletonComponent(App.getSingletonComponent())
                                  .build()
                                  .inject(this);
        presenter.setView(this);
    }

    @Override
    public void showTotal(String total) {
        totalView.setText(total);
    }

    @Override
    public void showTransactions(Collection<TransactionViewModel> transactions) {
        recyclerView.setAdapter(new TransactionsRecyclerViewAdapter(transactions));
    }
}
