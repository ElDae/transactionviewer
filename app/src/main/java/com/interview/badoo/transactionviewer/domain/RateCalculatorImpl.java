package com.interview.badoo.transactionviewer.domain;


import com.interview.badoo.transactionviewer.data.provider.DataProvider;
import com.interview.badoo.transactionviewer.data.Rate;
import com.interview.badoo.transactionviewer.domain.graph.Dijkstra;
import com.interview.badoo.transactionviewer.domain.graph.Edge;
import com.interview.badoo.transactionviewer.domain.graph.Graph;
import com.interview.badoo.transactionviewer.domain.graph.Vertex;
import com.interview.badoo.transactionviewer.util.Pair;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class RateCalculatorImpl implements RateCalculator {

    private DataProvider dataProvider;
    private Dijkstra dijkstra;
    private List<Rate> rates;
    private Graph graph;

    private Map<Pair<String, String>, Double> rateMap;

    @Inject
    public RateCalculatorImpl(DataProvider dataProvider, Dijkstra dijkstra) {
        this.dataProvider = dataProvider;
        this.dijkstra = dijkstra;

        rateMap = new HashMap<>();
    }

    @Override
    public double calculate(double amount, String from, String to) {
        if (rates == null) {
            rates = Arrays.asList(dataProvider.getRates());
            graph = Graph.from(rates);
        }

        Pair<String, String> path = Pair.create(from, to);
        Double rate = rateMap.get(path);
        if (rate == null) {
            dijkstra.calculateDistances(graph, new Vertex(from));
            List<Edge> shortestPath = dijkstra.getShortestPath(new Vertex(to));
            rate = 1d;
            for (Edge edge : shortestPath) {
                rate *= edge.getRate();
            }
            rateMap.put(path, rate);
        }

        return amount * rate;
    }
}
