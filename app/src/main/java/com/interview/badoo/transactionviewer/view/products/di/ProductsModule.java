package com.interview.badoo.transactionviewer.view.products.di;

import com.interview.badoo.transactionviewer.data.dao.TransactionDao;
import com.interview.badoo.transactionviewer.data.dao.TransactionDaoImpl;
import com.interview.badoo.transactionviewer.domain.interactor.ProductsInteractorImpl;
import com.interview.badoo.transactionviewer.view.ViewScope;
import com.interview.badoo.transactionviewer.view.products.ProductsMvp;
import com.interview.badoo.transactionviewer.view.products.ProductsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ProductsModule {

    @Provides
    @ViewScope
    TransactionDao providesTransactionDao(TransactionDaoImpl transactionDao) {
        return transactionDao;
    }

    @Provides
    @ViewScope
    ProductsMvp.Presenter providesPresenter(ProductsInteractorImpl productsInteractor) {
        return new ProductsPresenter(productsInteractor);
    }
}
