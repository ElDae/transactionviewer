package com.interview.badoo.transactionviewer.view.transactions;

import com.interview.badoo.transactionviewer.view.transactions.model.TransactionViewModel;

import java.util.Collection;

public interface TransactionsMVP {

    interface View {

        void showTotal(String total);
        void showTransactions(Collection<TransactionViewModel> transactions);
    }

    interface Presenter {

        void setView(View view);
        void init(String sku);
    }
}
