package com.interview.badoo.transactionviewer.view.transactions.di;

import com.interview.badoo.transactionviewer.data.dao.TransactionDao;
import com.interview.badoo.transactionviewer.data.dao.TransactionDaoImpl;
import com.interview.badoo.transactionviewer.domain.interactor.TransactionInteractorImpl;
import com.interview.badoo.transactionviewer.view.ViewScope;
import com.interview.badoo.transactionviewer.view.transactions.TransactionPresenter;
import com.interview.badoo.transactionviewer.view.transactions.TransactionsMVP;

import dagger.Module;
import dagger.Provides;

@Module
public class TransactionsModule {

    @Provides
    @ViewScope
    TransactionDao providesTransactionDao(TransactionDaoImpl transactionDao) {
        return transactionDao;
    }

    @Provides
    @ViewScope
    TransactionsMVP.Presenter providesPresenter(TransactionInteractorImpl interactor) {
        return new TransactionPresenter(interactor);
    }
}
