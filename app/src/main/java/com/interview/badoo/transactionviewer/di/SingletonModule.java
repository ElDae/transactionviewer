package com.interview.badoo.transactionviewer.di;

import android.content.Context;

import com.interview.badoo.transactionviewer.domain.RateCalculator;
import com.interview.badoo.transactionviewer.domain.RateCalculatorImpl;
import com.interview.badoo.transactionviewer.data.provider.DataProvider;
import com.interview.badoo.transactionviewer.data.provider.DataProviderJson;
import com.interview.badoo.transactionviewer.data.provider.JSONResourceReader;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SingletonModule {

    private Context appContext;

    public SingletonModule(Context appContext) {
        this.appContext = appContext;
    }

    @Provides
    @Singleton
    Context providesAppContext() {
        return appContext;
    }

    @Provides
    @Singleton
    JSONResourceReader providesJSONResourceReader(Context context) {
        return new JSONResourceReader(context);
    }

    @Provides
    @Singleton
    DataProvider providesDataProvider(DataProviderJson dataProviderJson) {
        return dataProviderJson;
    }

    @Provides
    @Singleton
    RateCalculator providesRateCalculator(RateCalculatorImpl rateCalculator) {
        return rateCalculator;
    }
}
