package com.interview.badoo.transactionviewer.view.products.di;

import com.interview.badoo.transactionviewer.di.SingletonComponent;
import com.interview.badoo.transactionviewer.view.ViewScope;
import com.interview.badoo.transactionviewer.view.products.ProductsActivity;

import dagger.Component;

@ViewScope
@Component(
        modules = ProductsModule.class,
        dependencies = SingletonComponent.class)
public interface ProductsComponent {

    void inject(ProductsActivity activity);
}
