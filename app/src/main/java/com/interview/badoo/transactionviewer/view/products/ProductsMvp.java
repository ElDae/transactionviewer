package com.interview.badoo.transactionviewer.view.products;

import com.interview.badoo.transactionviewer.view.products.model.ProductViewModel;

import java.util.Collection;

public interface ProductsMvp {

    interface View {

        void showProducts(Collection<ProductViewModel> products);
    }

    interface Presenter {

        void setView(View view);
        void init();
    }
}
