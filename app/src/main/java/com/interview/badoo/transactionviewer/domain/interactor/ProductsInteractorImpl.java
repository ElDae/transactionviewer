package com.interview.badoo.transactionviewer.domain.interactor;

import com.interview.badoo.transactionviewer.data.Transaction;
import com.interview.badoo.transactionviewer.data.dao.TransactionDao;
import com.interview.badoo.transactionviewer.view.products.model.ProductViewModel;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

public class ProductsInteractorImpl implements ProductsInteractor {

    private TransactionDao transactionDao;

    @Inject
    public ProductsInteractorImpl(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }

    @Override
    public Collection<ProductViewModel> getProductsForView() {
        Set<ProductViewModel> products = new HashSet<>();

        for (Transaction transaction : transactionDao.getTransactions()) {
            ProductViewModel product = new ProductViewModel(transaction.getSku());
            if (!products.add(product)) {
                product = findProduct(transaction.getSku(), products);
            }
            product.addTransaction();
        }

        return products;
    }

    private ProductViewModel findProduct(String sku, Set<ProductViewModel> products) {
        for (ProductViewModel product : products) {
            if (product.getSku().equals(sku)) {
                return product;
            }
        }

        throw new IllegalArgumentException("Wrong product sku: " + sku);
    }
}
