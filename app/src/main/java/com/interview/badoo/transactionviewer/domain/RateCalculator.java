package com.interview.badoo.transactionviewer.domain;

public interface RateCalculator {

    double calculate(double amount, String from, String to);
}
