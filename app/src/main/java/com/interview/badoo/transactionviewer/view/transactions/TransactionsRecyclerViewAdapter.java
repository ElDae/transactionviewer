package com.interview.badoo.transactionviewer.view.transactions;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.interview.badoo.transactionviewer.view.transactions.model.TransactionViewModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TransactionsRecyclerViewAdapter
        extends RecyclerView.Adapter<TransactionsRecyclerViewAdapter.ViewHolder> {

    private final List<TransactionViewModel> transactions;

    public TransactionsRecyclerViewAdapter(Collection<TransactionViewModel> transactions) {
        this.transactions = new ArrayList<>(transactions);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(android.R.layout.two_line_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        TransactionViewModel currentTransaction = transactions.get(position);
        holder.transaction = currentTransaction;
        holder.amountInGBP.setText(
                String.format("%s%s", currentTransaction.getCurrencySymbolGBP(), currentTransaction.getAmountInGBP()));
        holder.amountInOriginalCurrency.setText(
                String.format("%s%s", currentTransaction.getOriginalCurrencySymbol(), currentTransaction.getAmount()));
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView amountInGBP;
        public final TextView amountInOriginalCurrency;
        public TransactionViewModel transaction;

        public ViewHolder(View view) {
            super(view);
            amountInGBP = (TextView) view.findViewById(android.R.id.text1);
            amountInOriginalCurrency = (TextView) view.findViewById(android.R.id.text2);
        }
    }
}
