package com.interview.badoo.transactionviewer.data.provider;

import com.interview.badoo.transactionviewer.data.Rate;
import com.interview.badoo.transactionviewer.data.Transaction;

public interface DataProvider {

    Transaction[] getTransactions();

    Rate[] getRates();

}
