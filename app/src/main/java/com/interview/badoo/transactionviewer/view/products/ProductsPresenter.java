package com.interview.badoo.transactionviewer.view.products;

import com.interview.badoo.transactionviewer.domain.interactor.ProductsInteractor;

public class ProductsPresenter implements ProductsMvp.Presenter {

    private ProductsMvp.View view;
    private ProductsInteractor productsInteractor;

    public ProductsPresenter(ProductsInteractor productsInteractor) {
        this.productsInteractor = productsInteractor;
    }

    @Override
    public void setView(ProductsMvp.View view) {
        this.view = view;
    }

    @Override
    public void init() {
        view.showProducts(productsInteractor.getProductsForView());
    }
}
