package com.interview.badoo.transactionviewer.view.products;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.interview.badoo.transactionviewer.App;
import com.interview.badoo.transactionviewer.R;
import com.interview.badoo.transactionviewer.view.products.di.DaggerProductsComponent;
import com.interview.badoo.transactionviewer.view.products.model.ProductViewModel;

import java.util.Collection;

import javax.inject.Inject;

public class ProductsActivity extends AppCompatActivity implements ProductsMvp.View {

    @Inject ProductsMvp.Presenter presenter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependencies();
        initLayout();

        presenter.init();
    }

    private void initDependencies() {
        DaggerProductsComponent.builder()
                               .singletonComponent(App.getSingletonComponent())
                               .build()
                               .inject(this);
        presenter.setView(this);
    }

    private void initLayout() {
        setContentView(R.layout.products_layout);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.products));
        }
        recyclerView = ((RecyclerView) findViewById(R.id.item_list));
    }

    @Override
    public void showProducts(Collection<ProductViewModel> products) {
        recyclerView.setAdapter(new ProductsRecyclerViewAdapter(products));
    }
}
