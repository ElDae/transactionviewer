package com.interview.badoo.transactionviewer.di;

import com.interview.badoo.transactionviewer.domain.RateCalculator;
import com.interview.badoo.transactionviewer.data.provider.DataProvider;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = SingletonModule.class)
public interface SingletonComponent {

    DataProvider dataProvider();
    RateCalculator rateCalculator();
}
