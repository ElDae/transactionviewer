package com.interview.badoo.transactionviewer.data.dao;

import com.interview.badoo.transactionviewer.data.Transaction;

import java.util.List;

public interface TransactionDao {

    List<Transaction> getTransactions();

    List<Transaction> getTransactionsForProduct(String sku);
}
