package com.interview.badoo.transactionviewer.domain.interactor;

import com.interview.badoo.transactionviewer.view.transactions.model.TransactionViewModel;

import java.util.Collection;

public interface TransactionInteractor {

    Collection<TransactionViewModel> getTransactionsForView(String productSku);

    String getTotalAmountInGbpForMappedModels();
}
