package com.interview.badoo.transactionviewer.data.provider;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;

public class JSONResourceReader {

    private static final String TAG = JSONResourceReader.class.getSimpleName();
    private Context appContext;

    public JSONResourceReader(Context appContext) {
        this.appContext = appContext;
    }

    public <T> T parse(int id, Class<T> type) {
        Resources resources = appContext.getResources();
        InputStream resourceReader = resources.openRawResource(id);
        Writer writer = new StringWriter();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(resourceReader, "UTF-8"));
            String line = reader.readLine();
            while (line != null) {
                writer.write(line);
                line = reader.readLine();
            }
        } catch (Exception e) {
            Log.e(TAG, "Unhandled exception while using JSONResourceReader", e);
        } finally {
            try {
                resourceReader.close();
            } catch (Exception e) {
                Log.e(TAG, "Unhandled exception while using JSONResourceReader", e);
            }
        }

        Gson gson = new GsonBuilder().create();
        return gson.fromJson(writer.toString(), type);
    }
}
