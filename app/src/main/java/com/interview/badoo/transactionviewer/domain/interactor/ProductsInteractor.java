package com.interview.badoo.transactionviewer.domain.interactor;

import com.interview.badoo.transactionviewer.view.products.model.ProductViewModel;

import java.util.Collection;

public interface ProductsInteractor {

    Collection<ProductViewModel> getProductsForView();
}
