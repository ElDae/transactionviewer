package com.interview.badoo.transactionviewer.view.transactions.model;

public class TransactionViewModel {

    private String amount;
    private String amountInGBP;
    private String originalCurrencySymbol;
    private String currencySymbolGBP = "GBP";

    public TransactionViewModel(String amount, String amountInGBP, String originalCurrencySymbol) {
        this.amount = amount;
        this.amountInGBP = amountInGBP;
        this.originalCurrencySymbol = originalCurrencySymbol;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmountInGBP() {
        return amountInGBP;
    }

    public void setAmountInGBP(String amountInGBP) {
        this.amountInGBP = amountInGBP;
    }

    public String getOriginalCurrencySymbol() {
        return originalCurrencySymbol;
    }

    public void setOriginalCurrencySymbol(String originalCurrencySymbol) {
        this.originalCurrencySymbol = originalCurrencySymbol;
    }

    public String getCurrencySymbolGBP() {
        return currencySymbolGBP;
    }

    public void setCurrencySymbolGBP(String currencySymbolGBP) {
        this.currencySymbolGBP = currencySymbolGBP;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionViewModel that = (TransactionViewModel) o;

        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        if (amountInGBP != null ? !amountInGBP.equals(that.amountInGBP) : that.amountInGBP != null) return false;
        if (originalCurrencySymbol != null ? !originalCurrencySymbol.equals(that.originalCurrencySymbol) : that.originalCurrencySymbol != null)
            return false;
        return currencySymbolGBP != null ? currencySymbolGBP.equals(that.currencySymbolGBP) : that.currencySymbolGBP == null;

    }

    @Override
    public int hashCode() {
        int result = amount != null ? amount.hashCode() : 0;
        result = 31 * result + (amountInGBP != null ? amountInGBP.hashCode() : 0);
        result = 31 * result + (originalCurrencySymbol != null ? originalCurrencySymbol.hashCode() : 0);
        result = 31 * result + (currencySymbolGBP != null ? currencySymbolGBP.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TransactionViewModel{" +
                "amount=" + amount +
                ", amountInGBP=" + amountInGBP +
                ", originalCurrencySymbol='" + originalCurrencySymbol + '\'' +
                ", currencySymbolGBP='" + currencySymbolGBP + '\'' +
                '}';
    }
}
