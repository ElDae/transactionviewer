package com.interview.badoo.transactionviewer.domain.graph;

import com.interview.badoo.transactionviewer.data.Rate;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Graph {

    private Map<String, Vertex> vertices;
    private Set<Edge> edges;

    Graph() {
        vertices = new HashMap<>();
        edges = new HashSet<>();
    }

    public static Graph from(List<Rate> rates) {
        Graph graph = new Graph();

        for (Rate rate : rates) {
            Vertex from = new Vertex(rate.getFrom());
            Vertex to = new Vertex(rate.getTo());

            graph.addVertex(from);
            graph.addVertex(to);
            graph.addEdge(new Edge(from, to, rate.getRate()));
        }

        return graph;
    }

    public void addEdge(Edge edge) {
        edges.add(edge);
    }

    public void addVertex(Vertex vertex) {
        vertices.put(vertex.getName(), vertex);
    }

    public Collection<Vertex> getVertices() {
        return vertices.values();
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Graph graph = (Graph) o;

        if (vertices != null ? !vertices.equals(graph.vertices) : graph.vertices != null) return false;
        return edges != null ? edges.equals(graph.edges) : graph.edges == null;

    }

    @Override
    public int hashCode() {
        int result = vertices != null ? vertices.hashCode() : 0;
        result = 31 * result + (edges != null ? edges.hashCode() : 0);
        return result;
    }
}
