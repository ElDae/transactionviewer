package com.interview.badoo.transactionviewer.domain.interactor;

import com.interview.badoo.transactionviewer.data.Transaction;
import com.interview.badoo.transactionviewer.data.dao.TransactionDao;
import com.interview.badoo.transactionviewer.domain.RateCalculator;
import com.interview.badoo.transactionviewer.view.transactions.model.TransactionViewModel;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

public class TransactionInteractorImpl implements TransactionInteractor {

    private TransactionDao transactionDao;
    private RateCalculator rateCalculator;
    private DecimalFormat decimalFormat = new DecimalFormat("#.##");

    private double total;

    @Inject
    public TransactionInteractorImpl(TransactionDao transactionDao, RateCalculator rateCalculator) {
        this.transactionDao = transactionDao;
        this.rateCalculator = rateCalculator;
    }

    @Override
    public Collection<TransactionViewModel> getTransactionsForView(String sku) {
        List<Transaction> transactions = transactionDao.getTransactionsForProduct(sku);
        List<TransactionViewModel> transactionViewModels = new ArrayList<>();

        for (Transaction transaction : transactions) {
            double amount = transaction.getAmount();
            String originalCurrencySymbol = transaction.getCurrency();
            double amountInGBP = rateCalculator.calculate(amount, originalCurrencySymbol, "GBP");

            transactionViewModels.add(new TransactionViewModel(decimalFormat.format(amount),
                                                               decimalFormat.format(amountInGBP),
                                                               originalCurrencySymbol));

            total += amountInGBP;
        }

        return transactionViewModels;
    }

    @Override
    public String getTotalAmountInGbpForMappedModels() {
        return decimalFormat.format(total);
    }
}
