package com.interview.badoo.transactionviewer.view.products;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.interview.badoo.transactionviewer.view.products.model.ProductViewModel;
import com.interview.badoo.transactionviewer.view.transactions.TransactionsActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProductsRecyclerViewAdapter
        extends RecyclerView.Adapter<ProductsRecyclerViewAdapter.ViewHolder> {

    private final List<ProductViewModel> products;

    public ProductsRecyclerViewAdapter(Collection<ProductViewModel> products) {
        this.products = new ArrayList<>(products);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(android.R.layout.two_line_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ProductViewModel currentProduct = products.get(position);
        holder.product = currentProduct;
        holder.SKU.setText(currentProduct.getSku());
        holder.transactionNumber.setText(
                String.format("%s %s", currentProduct.getTransactionNumber(), "transactions"));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, TransactionsActivity.class);
                intent.putExtra(TransactionsActivity.ARG_SKU, holder.product.getSku());

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView SKU;
        public final TextView transactionNumber;
        public ProductViewModel product;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            SKU = (TextView) view.findViewById(android.R.id.text1);
            transactionNumber = (TextView) view.findViewById(android.R.id.text2);
        }
    }
}
