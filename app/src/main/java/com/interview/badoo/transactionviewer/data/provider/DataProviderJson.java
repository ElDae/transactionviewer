package com.interview.badoo.transactionviewer.data.provider;

import com.interview.badoo.transactionviewer.R;
import com.interview.badoo.transactionviewer.data.Rate;
import com.interview.badoo.transactionviewer.data.Transaction;

import javax.inject.Inject;

public class DataProviderJson implements DataProvider {

    private Transaction[] transactions;
    private Rate[] rates;
    private JSONResourceReader jsonResourceReader;

    @Inject
    public DataProviderJson(JSONResourceReader jsonResourceReader) {
        this.jsonResourceReader = jsonResourceReader;
    }

    @Override
    public Transaction[] getTransactions() {
        if (transactions == null || transactions.length == 0) {
            transactions = jsonResourceReader.parse(R.raw.transactions, Transaction[].class);
        }
        return transactions;
    }

    @Override
    public Rate[] getRates() {
        if (rates == null || rates.length == 0) {
            rates = jsonResourceReader.parse(R.raw.rates, Rate[].class);
        }
        return rates;
    }
}
