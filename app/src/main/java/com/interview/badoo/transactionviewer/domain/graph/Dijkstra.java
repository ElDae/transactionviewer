package com.interview.badoo.transactionviewer.domain.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

public class Dijkstra {

    private List<Vertex> nodes;
    private List<Edge> edges;

    private Set<Vertex> checked;
    private Set<Vertex> unchecked;

    private Map<Vertex, Vertex> predecessors;
    private Map<Vertex, Integer> distance;

    @Inject
    public Dijkstra() {
    }

    public void calculateDistances(Graph graph, Vertex from) {
        nodes = new ArrayList<>(graph.getVertices());
        edges = new ArrayList<>(graph.getEdges());

        checked = new HashSet<>();
        unchecked = new HashSet<>();

        distance = new HashMap<>();
        predecessors = new HashMap<>();

        calculateDistances(from);
    }

    public List<Edge> getShortestPath(Vertex to) {
        assertDistancesCalculated();

        List<Vertex> vertexPath = getVertexPath(to);
        List<Edge> pathEdge = new ArrayList<>();

        for (int i = 0; i < vertexPath.size(); i++) {
            if (isNotLast(vertexPath, i)) {
                pathEdge.add(findEdge(vertexPath.get(i), vertexPath.get(i + 1)));
            }
        }

        return pathEdge;
    }

    private void calculateDistances(Vertex from) {
        assertNodeExists(from);

        distance.put(from, 0);
        unchecked.add(from);
        while (unchecked.size() > 0) {
            Vertex node = getMinimum(unchecked);
            checked.add(node);
            unchecked.remove(node);
            findMinimalDistances(node);
        }
    }

    private void assertDistancesCalculated() {
        if (nodes == null) {
            throw new IllegalStateException("invoke calculateDistances() first");
        }
    }

    private List<Vertex> getVertexPath(Vertex to) {
        assertNodeExists(to);

        List<Vertex> path = new ArrayList<>();
        Vertex step = to;
        if (predecessors.get(step) == null) {
            return path;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        Collections.reverse(path);

        return path;
    }

    private void assertNodeExists(Vertex vertex) {
        Vertex existing = null;
        for (Vertex node : nodes) {
            if (node.equals(vertex)) {
                existing = node;
                break;
            }
        }
        if (existing == null) {
            throw new RuntimeException("Vertex does not exist in graph " + vertex.toString());
        }
    }

    private Vertex getMinimum(Set<Vertex> vertexes) {
        Vertex minimum = null;
        for (Vertex vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    private void findMinimalDistances(Vertex node) {
        List<Vertex> adjacentNodes = getNeighbors(node);
        for (Vertex target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node) + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node) + getDistance(node, target));
                predecessors.put(target, node);
                unchecked.add(target);
            }
        }
    }

    private int getShortestDistance(Vertex to) {
        Integer d = distance.get(to);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

    private int getDistance(Vertex node, Vertex target) {
        for (Edge edge : edges) {
            if (isEdgeBetweenNodes(node, target, edge)) {
                return edge.getWeight();
            }
        }
        throw new RuntimeException("Should not happen");
    }

    private boolean isEdgeBetweenNodes(Vertex node, Vertex target, Edge edge) {
        return edge.getFrom().equals(node) && edge.getTo().equals(target);
    }

    private List<Vertex> getNeighbors(Vertex node) {
        List<Vertex> neighbors = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.getFrom().equals(node) && !isSettled(edge.getTo())) {
                neighbors.add(edge.getTo());
            }
        }
        return neighbors;
    }

    private boolean isSettled(Vertex vertex) {
        return checked.contains(vertex);
    }

    private Edge findEdge(Vertex from, Vertex to) {
        for (Edge edge : edges) {
            if (edge.equals(new Edge(from, to, -1d))) {
                return edge;
            }
        }
        throw new RuntimeException("edge between " + from + " and " + to + " not found!");
    }

    private boolean isNotLast(List<Vertex> pathVertex, int i) {
        return i + 1 < pathVertex.size();
    }
}

