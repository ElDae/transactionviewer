package com.interview.badoo.transactionviewer.view.transactions.di;

import com.interview.badoo.transactionviewer.di.SingletonComponent;
import com.interview.badoo.transactionviewer.view.ViewScope;
import com.interview.badoo.transactionviewer.view.transactions.TransactionsActivity;

import dagger.Component;

@ViewScope
@Component(
        modules = TransactionsModule.class,
        dependencies = SingletonComponent.class)
public interface TransactionComponent {

    void inject(TransactionsActivity activity);
}
