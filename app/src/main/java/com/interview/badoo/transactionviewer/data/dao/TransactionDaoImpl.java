package com.interview.badoo.transactionviewer.data.dao;

import com.interview.badoo.transactionviewer.data.provider.DataProvider;
import com.interview.badoo.transactionviewer.data.Transaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

public class TransactionDaoImpl implements TransactionDao {

    private DataProvider dataProvider;

    @Inject
    public TransactionDaoImpl(DataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    @Override
    public List<Transaction> getTransactions() {
        return Arrays.asList(dataProvider.getTransactions());
    }

    @Override
    public List<Transaction> getTransactionsForProduct(String sku) {
        List<Transaction> transactionsForProduct = new ArrayList<>();
        for (Transaction transaction : Arrays.asList(dataProvider.getTransactions())) {
            if (transaction.getSku().equals(sku)){
                transactionsForProduct.add(transaction);
            }
        }
        return transactionsForProduct;
    }
}
