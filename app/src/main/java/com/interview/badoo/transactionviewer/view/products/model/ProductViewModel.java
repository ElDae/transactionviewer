package com.interview.badoo.transactionviewer.view.products.model;

public class ProductViewModel {

    public ProductViewModel(String sku) {
        this.sku = sku;
    }

    private String sku;
    private int transactionNumber;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getTransactionNumber() {
        return transactionNumber;
    }

    public void addTransaction() {
        transactionNumber++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductViewModel that = (ProductViewModel) o;

        return sku != null ? sku.equals(that.sku) : that.sku == null;

    }

    @Override
    public int hashCode() {
        return sku != null ? sku.hashCode() : 0;
    }
}
