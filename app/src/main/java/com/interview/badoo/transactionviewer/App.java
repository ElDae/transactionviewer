package com.interview.badoo.transactionviewer;

import android.app.Application;

import com.interview.badoo.transactionviewer.di.DaggerSingletonComponent;
import com.interview.badoo.transactionviewer.di.SingletonComponent;
import com.interview.badoo.transactionviewer.di.SingletonModule;

public class App extends Application {

    private static SingletonComponent singletonComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        singletonComponent = DaggerSingletonComponent.builder()
                                                     .singletonModule(new SingletonModule(this))
                                                     .build();
    }

    public static SingletonComponent getSingletonComponent() {
        return singletonComponent;
    }
}
