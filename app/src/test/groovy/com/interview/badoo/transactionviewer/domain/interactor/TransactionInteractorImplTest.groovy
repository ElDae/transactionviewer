package com.interview.badoo.transactionviewer.domain.interactor

import com.interview.badoo.transactionviewer.data.Transaction
import com.interview.badoo.transactionviewer.data.dao.TransactionDao
import com.interview.badoo.transactionviewer.domain.RateCalculator
import com.interview.badoo.transactionviewer.view.transactions.model.TransactionViewModel
import spock.lang.Specification

class TransactionInteractorImplTest extends Specification {

    TransactionDao transactionDaoMock = Mock(TransactionDao)
    RateCalculator rateCalculatorMock = Mock(RateCalculator)
    TransactionInteractorImpl interactor

    void setup() {
        interactor = new TransactionInteractorImpl(transactionDaoMock, rateCalculatorMock)
    }

    def "getTransactionsForView should return correct transactions"() {
        given:
        def sku = "sku"
        transactionDaoMock.getTransactionsForProduct(sku) >> [new Transaction(1, sku, "GBP"),
                                                              new Transaction(1, sku, "USD"),
                                                              new Transaction(1, sku, "PLN"),]
        and:
        rateCalculatorMock.calculate(1, "GBP", "GBP") >> 1
        rateCalculatorMock.calculate(1, "USD", "GBP") >> 0.5
        rateCalculatorMock.calculate(1, "PLN", "GBP") >> 2

        when:
        List result = new ArrayList<>(interactor.getTransactionsForView(sku))

        then:
        result == [new TransactionViewModel("1", "1", "GBP"),
                   new TransactionViewModel("1", "0,5", "USD"),
                   new TransactionViewModel("1", "2", "PLN")]

    }
}
