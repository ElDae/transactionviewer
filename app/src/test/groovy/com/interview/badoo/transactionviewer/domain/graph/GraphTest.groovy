package com.interview.badoo.transactionviewer.domain.graph

import com.interview.badoo.transactionviewer.data.Rate
import spock.lang.Specification

class GraphTest extends Specification {

    def "form should create correct graph"() {
        given:
        List<Rate> rates = new ArrayList<>();
        rates.add(new Rate("A", "B", 0.5));
        rates.add(new Rate("B", "A", 2));
        rates.add(new Rate("A", "C", 0.2));
        rates.add(new Rate("C", "A", 1.8));

        and:
        Graph graph = new Graph()
        graph.addVertex(new Vertex("A"))
        graph.addVertex(new Vertex("B"))
        graph.addVertex(new Vertex("C"))
        graph.addEdge(new Edge(new Vertex("A"), new Vertex("B"), 0.5d))
        graph.addEdge(new Edge(new Vertex("B"), new Vertex("A"), 2d))
        graph.addEdge(new Edge(new Vertex("A"), new Vertex("C"), 0.2d))
        graph.addEdge(new Edge(new Vertex("C"), new Vertex("A"), 1.8d))

        expect:
        Graph.from(rates) == graph
    }
}
