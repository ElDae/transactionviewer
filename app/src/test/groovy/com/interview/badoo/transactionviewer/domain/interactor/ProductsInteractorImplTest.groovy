package com.interview.badoo.transactionviewer.domain.interactor

import com.interview.badoo.transactionviewer.data.Transaction
import com.interview.badoo.transactionviewer.data.dao.TransactionDao
import com.interview.badoo.transactionviewer.view.products.model.ProductViewModel
import spock.lang.Specification

class ProductsInteractorImplTest extends Specification {

    TransactionDao transactionDaoMock
    ProductsInteractorImpl interactor

    void setup() {
        transactionDaoMock = Mock(TransactionDao)
        interactor = new ProductsInteractorImpl(transactionDaoMock)
    }

    def "getProductsForView should return correct products"() {
        given:
        transactionDaoMock.getTransactions() >> [getTransaction("t1"),
                                                 getTransaction("t2"),
                                                 getTransaction("t2"),
                                                 getTransaction("t3"),
                                                 getTransaction("t3"),
                                                 getTransaction("t3")]
        when:
        List<ProductViewModel> result = new ArrayList<>(interactor.getProductsForView())

        then:
        result == [new ProductViewModel("t1"), new ProductViewModel("t2"), new ProductViewModel("t3")]
    }

    def getTransaction(String productName) {
        new Transaction(1d, productName, "c1")
    }
}
