package com.interview.badoo.transactionviewer.domain.graph

import spock.lang.Specification
import spock.lang.Unroll

class DijkstraTest extends Specification {

    static Vertex vA = new Vertex("A")
    static Vertex vB = new Vertex("B")
    static Vertex vC = new Vertex("C")
    static Vertex vD = new Vertex("D")
    static Vertex vE = new Vertex("E")

    static Edge eAB = new Edge(vA, vB, 1d)
    static Edge eBA = new Edge(vB, vA, 1d)
    static Edge eBD = new Edge(vB, vD, 1d)
    static Edge eDB = new Edge(vD, vB, 1d)
    static Edge eBE = new Edge(vB, vE, 1d)
    static Edge eEB = new Edge(vE, vB, 1d)
    static Edge eCD = new Edge(vC, vD, 1d)
    static Edge eDC = new Edge(vD, vC, 1d)

    Graph graph

    void setup() {
        graph = new Graph()
        graph.addVertex(vA)
        graph.addVertex(vB)
        graph.addVertex(vC)
        graph.addVertex(vD)
        graph.addVertex(vE)

        graph.addEdge(eAB)
        graph.addEdge(eBA)
        graph.addEdge(eBD)
        graph.addEdge(eDB)
        graph.addEdge(eBE)
        graph.addEdge(eEB)
        graph.addEdge(eCD)
        graph.addEdge(eDC)
    }

    @Unroll
    def "getShortestPath from #from to #to should return #path"() {
        given:
        Dijkstra dijkstra = new Dijkstra()

        and:
        dijkstra.calculateDistances(graph, from)

        expect:
        dijkstra.getShortestPath(to) == path

        where:
        from | to | path
        vA   | vA | []
        vA   | vB | [eAB]
        vA   | vC | [eAB, eBD, eDC]
        vE   | vC | [eEB, eBD, eDC]
        vD   | vE | [eDB, eBE]
    }
}
