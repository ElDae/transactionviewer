package com.interview.badoo.transactionviewer.domain

import com.interview.badoo.transactionviewer.data.Rate
import com.interview.badoo.transactionviewer.data.provider.DataProvider
import com.interview.badoo.transactionviewer.domain.graph.Dijkstra
import spock.lang.Specification
import spock.lang.Unroll

class RateCalculatorImplTest extends Specification {

    DataProvider dataProviderMock = Mock(DataProvider)
    RateCalculatorImpl calculator

    void setup() {
        calculator = new RateCalculatorImpl(dataProviderMock, new Dijkstra())
    }

    @Unroll
    def "1 #from to #to should be calculated to #result"() {
        given:
        dataProviderMock.getRates() >> [new Rate("AUD", "USD", 0.5),
                                        new Rate("USD", "EUR", 0.5),
                                        new Rate("EUR", "GBP", 0.5)]

        expect:
        calculator.calculate(1, from, to) == result

        where:
        from  | to    | result
        "AUD" | "USD" | 0.5d
        "AUD" | "GBP" | 0.125d
        "EUR" | "GBP" | 0.5d
    }
}
